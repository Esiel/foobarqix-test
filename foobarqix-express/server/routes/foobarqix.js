const express = require('express')
const router = express.Router()
const { check, validationResult } = require('express-validator');


// VARIABLES

let foo = 'Foo';
let bar = 'Bar';
let qix = 'Qix';
let value = '';
let char = '';
let newValue = '';
let index, step;




router.get('/', (req, res) => {
    res.redirect("http://localhost:3000/")

})

router.post('/',
    (req, res, next) => {
        // VARIABLES
        res.startIndex = req.body.startIndex;
        res.endIndex = req.body.endIndex
        res.fooBarQix = []

        // LOGIC
        const changeValue = (string) => {
            value = "";
            if (parseInt(string) % 3 === 0) value += foo;
            if (parseInt(string) % 5 === 0) value += bar;
            if (parseInt(string) % 7 === 0) value += qix;

            for (step = 0; step < string.length; step++) {
                char = string.charAt(step);

                if (char === '3') value += foo;
                if (char === '5') value += bar;
                if (char === '7') value += qix;

                if (char === '0') {
                    let split = string.split('');
                    let findIndex = split.indexOf('0');
                    split.splice(findIndex, 1, '*');
                    string = split.join('');
                }

                if (value.match(/[a-zA-Z]/)) {
                    newValue = char.replace(/0/g, '*').replace(/[1-9]/g, '');
                    value += newValue;
                }
            }
            value === "" ? res.fooBarQix.push(string) : res.fooBarQix.push(value);
        }

        // INITIATE LOGIC   
        for (index = res.startIndex; index <= res.endIndex; index++) {
            let string = `${index}`
            changeValue(string)
        }
        next()
    },

    (req, res) => {
        // SEND RESPONSE
        res.json({
            startIndex: res.startIndex,
            endIndex: res.endIndex,
            data: res.fooBarQix,

        })
    }
)




module.exports = router