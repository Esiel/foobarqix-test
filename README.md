# FooBarQix-Wemanity

## client-side :
- only react was used
- includes tests
- start app :
    - cd foobarqix-react
    - npm i
    - npm start

## server-side :
- react for display & express for computing
- no tests 
- start app :
    - cd client 
    - npm i
    - npm start
    - cd server
    - npm i
    - npm start
