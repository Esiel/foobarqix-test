import React from "react";




const FooBarQix = ({ limit }) => {
    // VARIABLES
    let fooBarQix = [];
    let foo = 'Foo';
    let bar = 'Bar';
    let qix = 'Qix';
    let value = '';
    let char = '';
    let newValue = '';
    let index, step;

    // LOGIC
    const changeValue = (string) => {
        value = "";
        if (parseInt(string) % 3 === 0) value += foo;
        if (parseInt(string) % 5 === 0) value += bar;
        if (parseInt(string) % 7 === 0) value += qix;

        for (step = 0; step < string.length; step++) {
            char = string.charAt(step);

            if (char === '3') value += foo;
            if (char === '5') value += bar;
            if (char === '7') value += qix;

            if (char === '0') {
                let split = string.split('');
                let findIndex = split.indexOf('0');
                split.splice(findIndex, 1, '*');
                string = split.join('');
            }

            if (value.match(/[a-zA-Z]/)) {
                newValue = char.replace(/0/g, '*').replace(/[1-9]/g, '');
                value += newValue;
            }
        }
        value === "" ? fooBarQix.push(string) : fooBarQix.push(value);
    }

    // INITIATE LOGIC   
    for (index = 1; index <= limit; index++) {
        let string = `${index}`
        changeValue(string)
    }

    // DISPLAY LOGIC
    const display = fooBarQix.map((number, index) => {
        return (
            <div className="foobarqix-box" key={index}>
                <p className="foobarqix-input"> {index + 1} </p>
                <p className="foobarqix-change">  =>  </p>
                <p className="" >{number} </p>
            </div>
        )
    })
    fooBarQix = []

    // DISPLAY
    return (
        <div className="foobarqix-ctn">
            {display}
        </div>
    )
};

export default FooBarQix;




